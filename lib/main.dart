import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const flutterlayout());
}

class flutterlayout extends StatelessWidget {
  const flutterlayout({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.pink,
      ),
      home: const MyHomePage(title: 'First Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);



  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(

          title: Text(widget.title),
        ),
        body:Container(
          color: Colors.pinkAccent,
          constraints: BoxConstraints.expand(),
          child: SingleChildScrollView(
            child: Column(
              children: [ CircleAvatar(
                radius: 70,
                child: Icon(
                  Icons.account_box,
                  size: 100,
                ),
              ),
                Padding(
                    padding: const EdgeInsets.all(25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          "Row child 1",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "Row child 2",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,),
                        ),
                        Text(
                          "Row child 3",
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    )

                ),
                Padding(
                  padding: const EdgeInsets.all(30),
                  child: Text(
                    "This is Column",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextField(
                    controller: myController_user,
                    decoration: InputDecoration(
                      hintText: 'Username',
                      fillColor: Colors.white,
                      filled: true,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 15,right: 15,bottom: 30),
                  child: TextField(
                    controller: myController_pwd,
                    decoration: InputDecoration(
                      hintText: 'Password',
                      fillColor: Colors.white,
                      filled: true,
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: () {
                       myController_user.clear();
                       myController_pwd.clear();
                      },
                      child: Text('Cancel'),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.deepPurple,
                        onPrimary: Colors.cyanAccent,
                        padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                        textStyle: TextStyle(fontSize: 20),
                      ),

                    ),
                    ElevatedButton(
                      onPressed: () {
                        Fluttertoast.showToast(
                          msg: "hello, ${myController_user.text}",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.red,
                          textColor: Colors.white,
                          fontSize: 16.0
                      );

                      },
                      child: Text('Log in'),
                      style: ElevatedButton.styleFrom(
                        primary: Colors.cyanAccent,
                        onPrimary: Colors.deepPurple,
                        padding: EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                        textStyle: TextStyle(fontSize: 20),
                      ),

                    ),
                  ],
                ),
              ],
            ),
          ),
        )
    );
  }
}